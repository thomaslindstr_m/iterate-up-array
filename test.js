// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var iterateUpArray = require('./index.js');
    var assert = require('assert');

    describe('iterate-up-array', function() {
        var array = [1, 2, 3, 4, 5];

        it('should iterate up the arrays 5 indices', function() {
            var i = 0;

            iterateUpArray(array, function() {
                i++;
            });

            assert.equal(5, i);
        });

        it('should let the first iteration start at the front', function() {
            var first = null;

            iterateUpArray(array, function(index, i, end) {
                first = index;
                end();
            });

            assert.equal(1, first);
        });

        it('should be able to stop iteration', function() {
            var iterations = 0;

            iterateUpArray(array, function(index, i, end) {
                iterations++;
                end();
            });

            assert.equal(1, iterations);
        });

        it('should be able to return the data passed to the end function', function() {
            var data = iterateUpArray(array, function(index, i, end) {
                end('data');
            });

            assert.equal('data', data);
        });

        it('should be able to return an array of data if more arguments were passed', function() {
            var data = iterateUpArray(array, function(index, i, end) {
                end('data', 'is', 'very', 'cool');
            });

            assert.equal(4, Object.keys(data).length);
        });
    });
